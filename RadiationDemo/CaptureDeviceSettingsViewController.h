//
//  CaptureDeviceSetingsViewController.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/4/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "FXForms.h"
#import <AVFoundation/AVFoundation.h>

@interface CaptureDeviceSettingsViewController: FXFormViewController{

}

@property (nonatomic, weak) AVCaptureDevice *device;

@end
