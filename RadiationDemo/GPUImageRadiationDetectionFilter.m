//
//  GPURadiationDetectionFilter.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageRadiationDetectionFilter.h"
#import "GPUImageMaximumFilter.h"
#import "UIImage+Color.h"
#import <GPUImagePicture+TextureSubimage.h>
#import "GPUImageRadiationOutputFilter.h"

@interface GPUImageRadiationDetectionFilter()
{

}

@property (nonatomic, strong) GPUImageRadiationOutputFilter *outputFilter;
@property (nonatomic, strong) GPUImageMaximumFilter *maximumFilter;

@end

@implementation GPUImageRadiationDetectionFilter


#pragma mark -
#pragma mark Initialization and teardown

- (id)init;
{
    if (!(self = [super init]))
    {
        return nil;
    }
    
    self.outputFilter = [[GPUImageRadiationOutputFilter alloc] init];
    [self addFilter:_outputFilter];
    
    self.maximumFilter = [[GPUImageMaximumFilter alloc] init];
    
    __unsafe_unretained GPUImageRadiationDetectionFilter *weakSelf = self;
    
    [_maximumFilter setMaximumProcessingFinishedBlock:^(CGImageRef maxImage, CGImageRef max1Image) {
        
        if(weakSelf.maxPic){
            
            if(weakSelf.maxPic.outputImageSize.width != CGImageGetWidth(maxImage)){
            }
            
            [weakSelf.maxPic replaceTextureWithSubCGImage:maxImage];
            [weakSelf.max_1Pic replaceTextureWithSubCGImage:max1Image];
            [weakSelf.maxPic notifyTargetsAboutNewOutputTexture];
            [weakSelf.max_1Pic notifyTargetsAboutNewOutputTexture];
            
        }else{
            weakSelf.maxPic = [[GPUImagePicture alloc] initWithCGImage:maxImage];
            weakSelf.max_1Pic = [[GPUImagePicture alloc] initWithCGImage:max1Image];
            
            [weakSelf.maxPic addTarget:weakSelf.outputFilter atTextureLocation:0];
            [weakSelf.max_1Pic addTarget:weakSelf.outputFilter atTextureLocation:1];
            [weakSelf.maximumFilter addTarget:weakSelf.outputFilter atTextureLocation:2];
            
            [weakSelf.maxPic processImage];
            [weakSelf.max_1Pic processImage];
            
        }
        
        
    }];
    
   
    [self addFilter:_maximumFilter];

    
    self.initialFilters = [NSArray arrayWithObjects:_maximumFilter, nil];
    self.terminalFilter = _outputFilter;
    

    return self;
}

-(void)setNeedReset{
    
    runAsynchronouslyOnVideoProcessingQueue(^{
        [_maximumFilter setNeedReset:YES];
    });
    
}

@end


