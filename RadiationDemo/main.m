//
//  main.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/3/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
