//
//  GPURadiationDetectionFilter.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import <GPUImage/GPUImage.h>
#import <GLKit/GLKit.h>

@interface GPUImageRadiationDetectionFilter : GPUImageFilterGroup

@property (nonatomic, strong) GPUImagePicture *maxPic;
@property (nonatomic, strong) GPUImagePicture *max_1Pic;

-(void)setNeedReset;
@end
