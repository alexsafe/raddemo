//
//  GPUImageRadiationDetectionFilterVer2.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/23/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageRadiationDetectionFilterVer2.h"
#import <EXTScope.h>
#import "UIImage+Color.h"
#import "GPUImageSimpleDarkenBlendFilter.h"
#import <GPUImagePicture+TextureSubimage.h>
#import "GPUImageRadiationOutputFilter.h"

@interface GPUImageRadiationDetectionFilterVer2(){
    
}


@property (nonatomic, strong) GPUImageFilter *videoInput;
@property (nonatomic, strong) GPUImageFilter *output;

@property (nonatomic, strong) GPUImageFilter *value;
@property (nonatomic, strong) GPUImagePicture *maxPic;
@property (nonatomic, strong) GPUImagePicture *max_1Pic;
@property (nonatomic, strong) UIImage *maxImage;


@end

@implementation GPUImageRadiationDetectionFilterVer2{
}


#pragma mark -
#pragma mark Initialization and teardown

- (id)init;
{
    if (!(self = [super init]))
    {
        return nil;
    }
    
    _videoInput = [[GPUImageFilter alloc] init];
    [_videoInput useNextFrameForImageCapture];
    
    _output = [[GPUImageFilter alloc] init];

    
    __unsafe_unretained GPUImageRadiationDetectionFilterVer2 *weakSelf = self;
    
    [_videoInput setFrameProcessingCompletionBlock:^(GPUImageOutput *output, CMTime time) {
       
        [weakSelf setupChainWithOutput:output time:time];
        
    }];
    
   
    self.initialFilters = [NSArray arrayWithObjects:_videoInput, nil];
    self.terminalFilter = _output;
    
    
    return self;
}


-(void) setupChainWithOutput:(GPUImageOutput *)output time:(CMTime) time{
    if(self.maxPic == nil){
        
        UIImage *imageFromCurrentFramebuffer = [output imageFromCurrentFramebuffer];
        
        _value = [[GPUImageFilter alloc] init];
        
        _maxPic = [[GPUImagePicture alloc] initWithImage:imageFromCurrentFramebuffer] ;
        [_maxPic useNextFrameForImageCapture];
        
        _max_1Pic = [[GPUImagePicture alloc] initWithImage:[UIImage imageWithColor:[UIColor blackColor] size:imageFromCurrentFramebuffer.size]];
        [_max_1Pic useNextFrameForImageCapture];
        
        [_maxPic processImage];
        [_max_1Pic processImage];
        
        [_videoInput addTarget:_value];
        [_videoInput setFrameProcessingCompletionBlock:nil];

        //Calc New Max_1
        GPUImageLightenBlendFilter *calcNewMax1 = [[GPUImageLightenBlendFilter alloc] init];
        [_value addTarget:calcNewMax1];
        [_max_1Pic addTarget:calcNewMax1];
        
        //Calc Max
        GPUImageLightenBlendFilter *calcMax = [[GPUImageLightenBlendFilter alloc] init];
        
        [calcNewMax1 addTarget:calcMax];
        [_maxPic addTarget:calcMax];
        
        __unsafe_unretained GPUImageRadiationDetectionFilterVer2 *weakSelf = self;
        
        [calcMax setFrameProcessingCompletionBlock:^(GPUImageOutput *output, CMTime time) {

            runSynchronouslyOnVideoProcessingQueue(^{
                CGImageRef image= [self newCGImageWithOutput:output];
                UIImage  *test = [UIImage imageWithCGImage:image];
                CGImageRelease(image);
                
                [weakSelf.maxPic replaceTextureWithSubCGImage:image];
                [weakSelf.maxPic notifyTargetsAboutNewOutputTexture];
            });
        }];
        
        //Calc Max1
        GPUImageSimpleDarkenBlendFilter *calcMax1 = [[GPUImageSimpleDarkenBlendFilter alloc] init];

        [calcNewMax1 addTarget:calcMax1];
        [_maxPic addTarget:calcMax1];
        
        [calcMax1 setFrameProcessingCompletionBlock:^(GPUImageOutput *output, CMTime time) {
            
            runSynchronouslyOnVideoProcessingQueue(^{
                CGImageRef image= [self newCGImageWithOutput:output];
                
                [weakSelf.max_1Pic replaceTextureWithSubCGImage:image];
                CGImageRelease(image);
                
                [weakSelf.max_1Pic notifyTargetsAboutNewOutputTexture];
            });
        }];
        
        //Calc Differense
        GPUImageDifferenceBlendFilter *signal = [[GPUImageDifferenceBlendFilter alloc] init];
        [calcMax1 addTarget:signal];
        [calcMax addTarget:signal];
        
        [signal addTarget:_output];
        
        
    }

}


-(CGImageRef) newCGImageWithOutput:(GPUImageOutput*) output{
    NSUInteger totalNumberOfPixels = round(output->inputTextureSize.width * output->inputTextureSize.height);
    GLubyte * pixelBytes = (GLubyte *)malloc(totalNumberOfPixels * 4);
    
    [GPUImageContext useImageProcessingContext];
    [output->outputFramebuffer activateFramebuffer];
    glReadPixels(0, 0, (int)output->inputTextureSize.width, (int)output->inputTextureSize.height, GL_RGBA, GL_UNSIGNED_BYTE, pixelBytes);
    
    CGImageRef image= [self newCGImageWithData:pixelBytes size:output->inputTextureSize];
    return image;
}

-(CGImageRef) newCGImageWithData:(GLubyte *) data size:(CGSize) size{
    
    CGColorSpaceRef colorSpace=CGColorSpaceCreateDeviceRGB();
    CGContextRef bitmapContext=CGBitmapContextCreate(data, size.width, size.height, 8, 4*size.width, colorSpace,  kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault);
    CFRelease(colorSpace);
    CGImageRef cgImage=CGBitmapContextCreateImage(bitmapContext);
    CGContextRelease(bitmapContext);
    
    return cgImage;
}

@end
