//
//  UIImage+Color.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize) size;

@end
