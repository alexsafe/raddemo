//
//  CaptureDeviceSettingForm.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/4/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "CaptureDeviceSettingForm.h"

@interface CaptureDeviceSettingForm(){
    NSMutableArray *mdict;
}


@end

@implementation CaptureDeviceSettingForm

- (instancetype)init
{
    self = [super init];
    if (self) {
    
        @weakify(self);
        
        [[RACObserve(self, device) ignore:nil] subscribeNext:^(id x) {
            @strongify(self);
            
            [self reloadDeviceSettings];
        
            
            mdict= [NSMutableArray arrayWithCapacity:42];
            [mdict addObject:@"activeFormat"];
            
            [[RACObserve(self, activeFormat) ignore:nil] subscribeNext:^(id x) {
                @strongify(self);
                
                [self.device lockForConfiguration:nil];
                self.device.activeFormat = x;
                
                self.device.focusMode = self.focusMode;
                
                if (self.device.isSmoothAutoFocusSupported) {
                    self.device.smoothAutoFocusEnabled = self.smoothAutoFocusEnabled;
                }
                
                self.device.autoFocusRangeRestriction = self.autoFocusRangeRestriction;
                
                if([self.device isExposureModeSupported:self.exposureMode]){
                    self.device.exposureMode = self.exposureMode;
                }
                
                            
                if([self.device isWhiteBalanceModeSupported:self.whiteBalanceMode]){
                    self.device.whiteBalanceMode = self.whiteBalanceMode;
                }
                
                
                if(self.device.isLowLightBoostSupported){
                    self.device.automaticallyEnablesLowLightBoostWhenAvailable = self.automaticallyEnablesLowLightBoostWhenAvailable;
                }
                
                if (self.device.focusMode==AVCaptureFocusModeLocked) {
                    [self.device setFocusModeLockedWithLensPosition:self.lensPosition completionHandler:nil];
                }
                
                
                [self.device unlockForConfiguration];
                
            }];
            
            [mdict addObject:@"focusMode"];
            [[RACObserve(self, focusMode) ignore:nil] subscribeNext:^(id x) {
                @strongify(self);
                AVCaptureFocusMode mode = [x integerValue];
                
                [self.device lockForConfiguration:nil];
                self.device.focusMode = mode;
                [self.device unlockForConfiguration];
            }];
            
            
            
            
            if (self.device.isSmoothAutoFocusSupported) {
                [mdict addObject:@"smoothAutoFocusEnabled"];
                
                [[[RACObserve(self, smoothAutoFocusEnabled) ignore:nil] skip:1] subscribeNext:^(id x) {
                    @strongify(self);
                    [self.device lockForConfiguration:nil];
                    self.device.smoothAutoFocusEnabled = [x boolValue];
                    [self.device unlockForConfiguration];
                }];
            }
            
            if ( self.device.isAutoFocusRangeRestrictionSupported) {
                [mdict addObject:@"autoFocusRangeRestriction"];
                
                [[[RACObserve(self, autoFocusRangeRestriction) ignore:nil] skip:1] subscribeNext:^(id x) {
                    @strongify(self);
                    AVCaptureAutoFocusRangeRestriction rr = [x integerValue];
                    [self.device lockForConfiguration:nil];
                    self.device.autoFocusRangeRestriction = rr;
                    [self.device unlockForConfiguration];
                }];
            }
            
            
            [mdict addObject:@"exposureMode"];
            [[[RACObserve(self, exposureMode) ignore:nil] skip:1] subscribeNext:^(id x) {
                @strongify(self);
                AVCaptureExposureMode mode = [x integerValue];
                [self.device lockForConfiguration:nil];
                self.device.exposureMode = mode;
                [self.device unlockForConfiguration];
            }];
            
        
            
            [mdict addObject:@"exposureDuration"];
            [[RACObserve(self, exposureDuration) skip:1] subscribeNext:^(id x) {
                @strongify(self);
                CGFloat exposureDuration = [x floatValue];
                CMTime time = CMTimeMakeWithSeconds(exposureDuration, 1000000);
                
                time = CMTimeMaximum(time, self.device.activeFormat.minExposureDuration);
                time = CMTimeMinimum(time, self.device.activeFormat.maxExposureDuration);
                
                
                NSLog(@"%f",exposureDuration);
                CMTimeShow(time);

                [self.device lockForConfiguration:nil];
                
                [self.device setExposureModeCustomWithDuration:time
                                                           ISO: AVCaptureISOCurrent
                                             completionHandler:^(CMTime syncTime) {
                                                 self.exposureMode = self.device.exposureMode;
                                             }];
                
                [self.device unlockForConfiguration];
            }];
            
            [mdict addObject:@"ISO"];
            [[RACObserve(self, ISO) skip:1] subscribeNext:^(id x) {
                @strongify(self);
                CGFloat ISO = [x floatValue];
                
                ISO = fmaxf(ISO, self.device.activeFormat.minISO);
                ISO = fminf(ISO, self.device.activeFormat.maxISO);
                
                [self.device lockForConfiguration:nil];
                
                [self.device setExposureModeCustomWithDuration:AVCaptureExposureDurationCurrent
                                                           ISO: ISO
                                             completionHandler:^(CMTime syncTime) {
                                                 self.exposureMode = self.device.exposureMode;
                                             }];
                
                [self.device unlockForConfiguration];
            }];
            
            
            
            [mdict addObject:@"whiteBalanceMode"];
            [[[RACObserve(self, whiteBalanceMode) ignore:nil] skip:1] subscribeNext:^(id x) {
                @strongify(self);
                AVCaptureWhiteBalanceMode mode = [x integerValue];
                [self.device lockForConfiguration:nil];
                if ([self.device isWhiteBalanceModeSupported:mode]) {
                    self.device.whiteBalanceMode = mode;
                }
                [self.device unlockForConfiguration];
            }];
            
            [mdict addObject:@"videoZoomFactor"];
            [[[RACObserve(self, videoZoomFactor) ignore:0] skip:1] subscribeNext:^(id x) {
                @strongify(self);
                [self.device lockForConfiguration:nil];
                self.device.videoZoomFactor = [x floatValue];
                [self.device unlockForConfiguration];
            }];
            
            if (self.device.isLowLightBoostSupported) {
                [mdict addObject:@"automaticallyEnablesLowLightBoostWhenAvailable"];
                
                [[[RACObserve(self, automaticallyEnablesLowLightBoostWhenAvailable) ignore:nil] skip:1] subscribeNext:^(id x) {
                    @strongify(self);
                    [self.device lockForConfiguration:nil];
                    self.device.videoZoomFactor = [x boolValue];
                    [self.device unlockForConfiguration];
                }];
            }
            
            
            [mdict addObject:@"lensPosition"];
            
            [[[RACObserve(self, lensPosition) ignore:nil] skip:1] subscribeNext:^(id x) {
                @strongify(self);
                
                [self.device lockForConfiguration:nil];
                [self.device setFocusModeLockedWithLensPosition:[x floatValue] completionHandler:^(CMTime syncTime) {
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateFields" object:nil];
                    _focusMode=self.device.focusMode;
                }];
                [self.device unlockForConfiguration];
                
            }];
            
            
            
            
            
            if (self.device.activeFormat.isVideoHDRSupported) {
                
                [mdict addObject:@"automaticallyAdjustsVideoHDREnabled"];
                
                [[RACObserve(self, videoHDREnabled) skip:1] subscribeNext:^(id x) {
                    @strongify(self);
                    
                    [self.device lockForConfiguration:nil];
                    [self.device setAutomaticallyAdjustsVideoHDREnabled:[x boolValue]];
                    [self.device unlockForConfiguration];
                    
                }];
                
                [mdict addObject:@"videoHDREnabled"];
                
                [[RACObserve(self, videoHDREnabled) skip:1] subscribeNext:^(id x) {
                    @strongify(self);
                    
                    [self.device lockForConfiguration:nil];
                    [self.device setAutomaticallyAdjustsVideoHDREnabled:NO];
                    [self.device setVideoHDREnabled:[x boolValue]];
                    [self.device unlockForConfiguration];
                    
                }];
            }
            

            
            
        }];
        
    }
    
    return self;
}


-(void) reloadDeviceSettings{
    _activeFormat = self.device.activeFormat;
    _focusMode=self.device.focusMode;
    _smoothAutoFocusEnabled = self.device.smoothAutoFocusEnabled;
    _autoFocusRangeRestriction = self.device.autoFocusRangeRestriction;
    
    _exposureMode = self.device.exposureMode;
    _exposureDuration = CMTimeGetSeconds(self.device.exposureDuration);
    _ISO = self.device.ISO;
    
    _whiteBalanceMode = self.device.whiteBalanceMode;
    
    _automaticallyEnablesLowLightBoostWhenAvailable = self.device.automaticallyEnablesLowLightBoostWhenAvailable;

    _videoZoomFactor = self.device.videoHDREnabled;
    _lensPosition = self.device.lensPosition;
    _videoHDREnabled = self.device.videoHDREnabled;
    _automaticallyAdjustsVideoHDREnabled = self.device.automaticallyAdjustsVideoHDREnabled;
    
}

- (NSArray *)fields
{
    
    if (self.device==nil) {
        return @[];
    }
    
    return mdict;
}

- (NSDictionary *)activeFormatField
{
//    UILabel *textLabel;
//    textLabel.font=[UIFont systemFontOfSize:12.0]

    NSArray *formats = self.device.formats;
    
    return @{FXFormFieldTitle: @"Active Format",
             FXFormFieldHeader: @"Managing Formats",
             FXFormFieldType:FXFormFieldTypeLabel,
             FXFormFieldOptions:formats?formats:@[@"w"],
             FXFormFieldValueTransformer:^(AVCaptureDeviceFormat *input) {
                 CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions(input.formatDescription);
                 AVFrameRateRange *fpsRange = input.videoSupportedFrameRateRanges.firstObject;

                 
                 NSString *value = [NSString stringWithFormat:@"%dx%d, fps:%.0f-%.0f, iso:%.0f-%.0f,fov:%.2f, %@",
                                    dimensions.width,
                                    dimensions.height,
                                    fpsRange.minFrameRate,
                                    fpsRange.maxFrameRate,
                                    input.minISO,
                                    input.maxISO,
                                    input.videoFieldOfView,
                                    input.videoHDRSupported?@"HDR":@""
                                    ];

                 
                 
                 return value;
             },
             @"textLabel.font":[UIFont systemFontOfSize:12.0],
             @"detailTextLabel.font":[UIFont systemFontOfSize:12.0]
             
             };
}

-(NSDictionary *) focusModeField{
    return @{
             FXFormFieldHeader: @"Managing Focus Settings",
             FXFormFieldOptions:@[
                     @"Locked",
                     @"Auto Focus",
                     @"Continuous Auto Focus",
                     ],
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             @"detailTextLabel.font":[UIFont systemFontOfSize:12.0]};

}

-(NSDictionary *) smoothAutoFocusEnabledField{
    return @{
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             @"detailTextLabel.font":[UIFont systemFontOfSize:12.0]};
    
}

-(NSDictionary *) autoFocusRangeRestrictionField{
    return @{
             FXFormFieldOptions:@[
                     @"None",
                     @"Near",
                     @"Far",
                     ],
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             @"detailTextLabel.font":[UIFont systemFontOfSize:12.0]};
    
}

-(NSDictionary *) exposureModeField{
    return @{
            FXFormFieldHeader: @"Managing Image Exposure",
            FXFormFieldOptions:@[
                     @"Locked",
                     @"Auto Expose",
                     @"Continuous Auto Exposure",
                     @"Custom",
                     ],
            @"textLabel.font":[UIFont systemFontOfSize:14.0],
            @"detailTextLabel.font":[UIFont systemFontOfSize:12.0]};
    
}

-(NSDictionary *) exposureDurationField{

    Float64 minExposureDuration = CMTimeGetSeconds(self.device.activeFormat.minExposureDuration);
    Float64 maxExposureDuration = CMTimeGetSeconds(self.device.activeFormat.maxExposureDuration);
    
    NSLog(@"%f - %f",minExposureDuration,maxExposureDuration);
    return @{
             FXFormFieldCell:[FXFormSliderCell class],
             @"slider.minimumValue":@(minExposureDuration),
             @"slider.maximumValue":@(maxExposureDuration),
             @"slider.continuous":@(NO),
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             };
}

-(NSDictionary *) ISOField{
    
    float minISO = self.device.activeFormat.minISO;
    float msxISO = self.device.activeFormat.maxISO;
    
    return @{
             FXFormFieldCell:[FXFormSliderCell class],
             @"slider.minimumValue":@(minISO),
             @"slider.maximumValue":@(msxISO),
             @"slider.continuous":@(NO),
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             };
}

-(NSDictionary *) whiteBalanceModeField{
    return @{
             FXFormFieldHeader: @"Managing the White Balance",
             FXFormFieldOptions:@[
                     @"Locked",
                     @"Auto White Balance",
                     @"Continuous Auto White Balance",
                     ],
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             @"detailTextLabel.font":[UIFont systemFontOfSize:12.0]};
    
}

-(NSDictionary *) videoZoomFactorField{
    return @{
             FXFormFieldHeader: @"Managing Zoom Settings",
             FXFormFieldCell:[FXFormSliderCell class],
             @"slider.minimumValue":@(1.0),
             @"slider.maximumValue":@(self.device.activeFormat.videoMaxZoomFactor),
             @"slider.continuous":@(NO),
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             };
    
}


-(NSDictionary *)automaticallyEnablesLowLightBoostWhenAvailableField{
    return @{
             FXFormFieldHeader:@"Managing Low Light Settings",
             };
}

-(NSDictionary *) lensPositionField{
    return @{
             FXFormFieldHeader: @"Managing the Lens Position",
             FXFormFieldCell:[FXFormSliderCell class],
             @"slider.minimumValue":@(0.0),
             @"slider.maximumValue":@(1.0),
             @"slider.continuous":@(YES),
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             };
    
}


-(NSDictionary *)automaticallyAdjustsVideoHDREnabledField{
    return @{
             FXFormFieldHeader: @"Managing High Dynamic Range Video",
            FXFormFieldAction: @"updateFields",
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             @"detailTextLabel.font":[UIFont systemFontOfSize:12.0]
             };
    
}

-(NSDictionary *)videoHDREnabledField{
    return @{
             FXFormFieldAction: @"updateFields",
             @"textLabel.font":[UIFont systemFontOfSize:14.0],
             @"detailTextLabel.font":[UIFont systemFontOfSize:12.0]
            };
    
}






@end
