//
//  GPUImageMaximumThresholdFilter.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import <GPUImage/GPUImage.h>

@interface GPUImageMaximumThresholdFilter_ : GPUImageFilter{
    GLint maxUniform;
    GLint max_1Uniform;
}

@property(readwrite, nonatomic) GPUVector4 max;
@property(readwrite, nonatomic) GPUVector4 max_1;

@end
