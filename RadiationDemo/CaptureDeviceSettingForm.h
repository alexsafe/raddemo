//
//  CaptureDeviceSettingForm.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/4/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>

#import <FXForms/FXForms.h>
#import <ReactiveCocoa.h>
#import <libextobjc/EXTScope.h>


@interface CaptureDeviceSettingForm : NSObject<FXForm>{
//    AVCaptureDeviceFormat *activeFormat;

}

@property (nonatomic, weak) AVCaptureDevice *device;

//Managing Formats
@property (nonatomic, strong) AVCaptureDeviceFormat *activeFormat;

//Managing Focus Settings
@property (nonatomic, assign) AVCaptureFocusMode focusMode;
@property (nonatomic, assign) BOOL smoothAutoFocusEnabled;
@property (nonatomic, assign) AVCaptureAutoFocusRangeRestriction autoFocusRangeRestriction;

//Managing Exposure Settings
@property (nonatomic, assign) AVCaptureExposureMode exposureMode;
@property (nonatomic, assign) AVCaptureWhiteBalanceMode whiteBalanceMode;

@property(nonatomic, assign) CGFloat exposureDuration;
@property(nonatomic, assign) float ISO;

//Managing Low Light Settings
//@property (nonatomic, assign) BOOL lowLightBoostEnabled;
@property (nonatomic, assign) BOOL automaticallyEnablesLowLightBoostWhenAvailable;

//Managing Zoom Settings
@property (nonatomic, assign) CGFloat videoZoomFactor;


//Managing the Lens Position
@property(nonatomic, assign) float lensPosition;

//Managing High Dynamic Range Video
@property(nonatomic, getter=isVideoHDREnabled) BOOL automaticallyAdjustsVideoHDREnabled;
@property(nonatomic, getter=isVideoHDREnabled) BOOL videoHDREnabled;

-(void) reloadDeviceSettings;


@end
