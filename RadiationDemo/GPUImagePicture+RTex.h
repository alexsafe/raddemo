//
//  GPUImagePicture+RTex.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImagePicture.h"

@interface GPUImagePicture (RTex)

- (void)replaceTextureWithData:(GLubyte *)imageData inRect:(CGRect)subRect;

@end
