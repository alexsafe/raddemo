//
//  GPUImageMaximumFilter.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageMaximumFilter.h"
#import "UIImage+Color.h"
#import <EXTScope.h>
#import <GPUImagePicture+TextureSubimage.h>
#import "GPUImagePicture+RTex.h"


NSString *const kGPUImageMaximumFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
 {
     mediump vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     gl_FragColor = textureColor;
 }
 );


@interface GPUImageMaximumFilter(){
}

@property (nonatomic, strong) GPUImagePicture *testPicture;

- (void)extractMaximumsFormOutput:(GPUImageOutput *)output atFrameTime:(CMTime)frameTime;
@end

@implementation GPUImageMaximumFilter{
    __block GLubyte *value;
    __block GLubyte *max;
    __block GLubyte *max_1;
    
}

- (void)dealloc
{
    free(value);
    free(max);
    free(max_1);
    
}


- (id)init;
{
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageMaximumFragmentShaderString]))
    {
        return nil;
    }
    
   
    @weakify(self);
    [self setFrameProcessingCompletionBlock:^(GPUImageOutput *output, CMTime frameTime) {
        runSynchronouslyOnVideoProcessingQueue(^{
            @strongify(self)
            [self extractMaximumsFormOutput:output atFrameTime:frameTime];
        });
    }];
    
    return self;
}



- (void)extractMaximumsFormOutput:(GPUImageOutput *)output atFrameTime:(CMTime)frameTime;
{
    runSynchronouslyOnVideoProcessingQueue(^{
        
        NSUInteger totalNumberOfPixels = round(output->inputTextureSize.width * output->inputTextureSize.height);
        BOOL firstStep = NO;
        
        if (_needReset){
            free(max);
            free(max_1);
            
            max = NULL;
            max_1 = NULL;
        }
        
        if (max==NULL && max_1==NULL) {
            max = (GLubyte *)malloc(totalNumberOfPixels * 4);
            max_1 = (GLubyte *)malloc(totalNumberOfPixels * 4);
            firstStep = YES;
        }
        
        if (value == NULL)
        {
            value = (GLubyte *)malloc(totalNumberOfPixels * 4);
        }
        
        [GPUImageContext useImageProcessingContext];
        [output->outputFramebuffer activateFramebuffer];
        glReadPixels(0, 0, (int)output->inputTextureSize.width, (int)output->inputTextureSize.height, GL_RGBA, GL_UNSIGNED_BYTE, value);
        
        void (^processAtIndex)(NSUInteger index) = ^(NSUInteger index) {
            
            NSUInteger valueC = value[index];
            NSUInteger maxC   = max[index];
            NSUInteger max_1C   = max_1[index];
            
            if(valueC>max_1C){
                max_1C=valueC;
                
                if (max_1C>maxC) {
                    NSUInteger tmp = max_1C;
                    max_1C = maxC;
                    maxC = tmp;
                }
                
            }
            
            max_1[index] = max_1C;
            max[index] = maxC;
            
        };

        for (NSUInteger currentPixel = 0; currentPixel < totalNumberOfPixels; currentPixel++)
        {
            
            if (firstStep || _needReset) {
                max[4*currentPixel]   = 0;
                max[4*currentPixel+1] = 0;
                max[4*currentPixel+2] = 0;
                max[4*currentPixel+3] = 255;

                max_1[4*currentPixel] = 0;
                max_1[4*currentPixel+1] = 0;
                max_1[4*currentPixel+2] = 0;
                max_1[4*currentPixel+3] = 255;
                _needReset = NO;

            }else{
                
                processAtIndex(4*currentPixel);
                processAtIndex(4*currentPixel+1);
                processAtIndex(4*currentPixel+2);
                
            }
            
        }
        
        if (_maximumProcessingFinishedBlock != NULL)
        {
            
            CGImageRef maxCGImage= [self newCGImageWithData:max size:output->inputTextureSize];
            CGImageRef max1CGImage= [self newCGImageWithData:max_1 size:output->inputTextureSize];
            
            _maximumProcessingFinishedBlock(maxCGImage, max1CGImage);
            CGImageRelease(maxCGImage);
            CGImageRelease(max1CGImage);
        }
        

    });
}

-(CGImageRef) newCGImageWithData:(GLubyte *) data size:(CGSize) size{
    
    CGColorSpaceRef colorSpace=CGColorSpaceCreateDeviceRGB();
    CGContextRef bitmapContext=CGBitmapContextCreate(data, size.width, size.height, 8, 4*size.width, colorSpace,  kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault);
    CFRelease(colorSpace);
    CGImageRef cgImage=CGBitmapContextCreateImage(bitmapContext);
    CGContextRelease(bitmapContext);
    
    return cgImage;
}

@end
