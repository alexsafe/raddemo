//
//  GPUImageRadFilter.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/16/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageFilter.h"
#import <GLKit/GLKit.h>

extern NSString *const kGPUImageRadFragmentShaderString;



@interface GPUImageMaximumFilter_ : GPUImageFilter

@property(nonatomic, copy) void(^colorMaximumProcessingFinishedBlock)(GPUVector4 max, GPUVector4 max_1, CMTime frameTime);


@end
