//
//  GPUImageSimpleDarkenBlendFilter.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/23/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageTwoInputFilter.h"

@interface GPUImageSimpleDarkenBlendFilter : GPUImageTwoInputFilter

@end
