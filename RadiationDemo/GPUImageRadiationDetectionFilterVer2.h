//
//  GPUImageRadiationDetectionFilterVer2.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/23/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import <GPUImage/GPUImage.h>

@interface GPUImageRadiationDetectionFilterVer2 : GPUImageFilterGroup

@end
