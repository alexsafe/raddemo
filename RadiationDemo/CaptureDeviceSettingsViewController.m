//
//  CaptureDeviceSetingsViewController.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/4/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "CaptureDeviceSettingsViewController.h"
#import "CaptureDeviceSettingForm.h"
#import <ReactiveCocoa.h>

@implementation CaptureDeviceSettingsViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib
{
    //set up form
    self.formController.form = [[CaptureDeviceSettingForm alloc] init];
    
    CaptureDeviceSettingForm *form = (CaptureDeviceSettingForm*) self.formController.form;
    
    RACSignal *deviceSignal = [RACObserve(self, device) ignore:nil];
    
    RAC(form, device) = deviceSignal;
    
    @weakify(self);
    [[RACSignal combineLatest:@[
                               deviceSignal,
                               [RACObserve(self.formController, tableView) ignore:nil]
                               ]]
     subscribeNext:^(id x) {
        @strongify(self);
         self.tableView.backgroundColor = [UIColor clearColor];
        [self updateFields];
     }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFields) name:@"UpdateFields" object:nil];
    
    
    
}

- (void)updateFields
{
    //refresh the form
    self.formController.form = self.formController.form;
    [(CaptureDeviceSettingForm*)self.formController.form reloadDeviceSettings];
    [self.tableView reloadData];
}


@end
