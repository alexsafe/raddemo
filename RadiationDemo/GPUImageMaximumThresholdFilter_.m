//
//  GPUImageMaximumThresholdFilter.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageMaximumThresholdFilter_.h"

NSString *const kGPUImageMaximumThresholdFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 uniform highp vec4 max;
 uniform highp vec4 max_1;
 
 
 void main()
 {
     highp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
//     highp float luminance = dot(textureColor.rgb, W);
//     highp float thresholdResult = step(threshold, luminance);
     
     gl_FragColor = max-max_1;
 }
 );


@implementation GPUImageMaximumThresholdFilter_

#pragma mark -
#pragma mark Initialization

- (id)init;
{
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageMaximumThresholdFragmentShaderString]))
    {
        return nil;
    }
    
    maxUniform = [filterProgram uniformIndex:@"max"];
    max_1Uniform = [filterProgram uniformIndex:@"max_1"];

    
    _max = (GPUVector4){.one=0.0,.two=0.0,.three=0.0, .four=0.0};
    _max_1 = (GPUVector4){.one=0.0,.two=0.0,.three=0.0, .four=0.0};
    
    return self;
}

#pragma mark -
#pragma mark Accessors

- (void)setMax:(GPUVector4)max
{
    _max = max;
  
    [self setFloatVec4:_max forUniform:@"max"];
}

- (void)setMax_1:(GPUVector4)max_1{
    _max_1 = max_1;
    
    [self setFloatVec4:_max_1 forUniform:@"max_1"];
}

@end
