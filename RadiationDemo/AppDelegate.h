//
//  AppDelegate.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/3/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

