//
//  GPUImageMaximumFilter.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageThreeInputFilter.h"
#import <GPUImage/GPUImage.h>

typedef struct {
    NSUInteger r, g, b, a;
} RGBAColor;

@interface GPUImageMaximumFilter : GPUImageFilter

@property (nonatomic, assign) BOOL needReset;
@property(nonatomic, copy) void(^maximumProcessingFinishedBlock)(CGImageRef maxImageRef, CGImageRef max1ImageRef);

@end
