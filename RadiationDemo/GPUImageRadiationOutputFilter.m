//
//  GPUImageRadiationOutputFilter.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/22/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageRadiationOutputFilter.h"


NSString *const kPUImageRadiationOutputFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 varying highp vec2 textureCoordinate2;
 varying highp vec2 textureCoordinate3;
 
 uniform sampler2D inputImageTexture; //max
 uniform sampler2D inputImageTexture2; // max_1
 uniform sampler2D inputImageTexture3;
 
 void main()
 {
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     lowp vec4 textureColor2 = texture2D(inputImageTexture2, textureCoordinate2);
     lowp vec4 textureColor3 = texture2D(inputImageTexture3, textureCoordinate3);
     
     gl_FragColor =  textureColor-textureColor2;
 }
 );

@implementation GPUImageRadiationOutputFilter

- (id)init;
{
    if (!(self = [super initWithFragmentShaderFromString:kPUImageRadiationOutputFragmentShaderString]))
    {
        return nil;
    }
    
    return self;
}


@end
