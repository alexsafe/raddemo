//
//  GPUImagePicture+RTex.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/21/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImagePicture+RTex.h"

@implementation GPUImagePicture (RTex)


- (void)replaceTextureWithData:(GLubyte *)imageData inRect:(CGRect)subRect {

    
    // We don't have to worry about scaling the subimage or finding a power of two size.
    // The initialization has taken care of that for us.
    pixelSizeOfImage = subRect.size;
    
    dispatch_semaphore_signal(imageUpdateSemaphore);
        
    runSynchronouslyOnVideoProcessingQueue(^{
        [GPUImageContext useImageProcessingContext];
        [outputFramebuffer disableReferenceCounting];
        
        glBindTexture(GL_TEXTURE_2D, [outputFramebuffer texture]);
        
        // no need to use self.outputTextureOptions here since pictures need this texture formats and type
        glTexSubImage2D(GL_TEXTURE_2D, 0, subRect.origin.x, subRect.origin.y, (GLint)subRect.size.width, subRect.size.height, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
        
        if (self.shouldSmoothlyScaleOutput)
        {
            glGenerateMipmap(GL_TEXTURE_2D);
        }
        glBindTexture(GL_TEXTURE_2D, 0);
    });
    
}

@end
