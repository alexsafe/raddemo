//
//  GPUImageRadiationOutputFilter.h
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/22/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import <GPUImage.h>

@interface GPUImageRadiationOutputFilter : GPUImageThreeInputFilter

@end
