//
//  ViewController.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/3/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "CameraViewController.h"
#import <GPUImage/GPUImage.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "CaptureDeviceSettingsViewController.h"
#import "GPUImageRadiationDetectionFilter.h"
#import "GPUImageRadiationDetectionFilterVer2.h"

@interface CameraViewController (){
    __weak IBOutlet GPUImageView *imageView;
    __weak IBOutlet GPUImageView *sample1ImageView;
    __weak IBOutlet GPUImageView *sample2ImageView;
    __weak IBOutlet GPUImageView *sample3ImageView;
    __weak IBOutlet UILabel *statusLabel;
    __weak IBOutlet NSLayoutConstraint *settingsContainerHeightConstraint;
    __weak IBOutlet UIButton *resetBtn;
    
    GPUImageRadiationDetectionFilter *radFilter;
}

@property (nonatomic, strong) GPUImageVideoCamera *videoCamera;
@property (nonatomic, strong) GPUImageMovie *testMoview;
@property (nonatomic, strong) GPUImagePicture *testPicture;

@end

@implementation CameraViewController

-(void)loadView{
    [super loadView];
    settingsContainerHeightConstraint.constant= 44;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    
#if TARGET_IPHONE_SIMULATOR
    
    self.testMoview = [[GPUImageMovie alloc] initWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Untitled1" ofType:@"mp5"]]];
    [self.testMoview setShouldRepeat:YES];
    
    self.testPicture = [[GPUImagePicture alloc] initWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"IMG_4307" ofType:@"JPG"]]];
    
    GPUImageRadiationDetectionFilterVer2 *filter = [[GPUImageRadiationDetectionFilterVer2 alloc] init];

    [_testMoview addTarget:filter];
    
    [filter addTarget:imageView];

    [_testPicture processImage];
    [_testMoview startProcessing];

    [_testMoview addTarget:sample1ImageView];


    
#else
    
    self.videoCamera = [[GPUImageVideoCamera alloc]
                        initWithSessionPreset:AVCaptureSessionPresetHigh
                        cameraPosition:AVCaptureDevicePositionBack];
    _videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;

    [_videoCamera startCameraCapture];
    [_videoCamera addTarget:sample1ImageView];

    [self configureUI];

#endif
    
}

-(void) setupFilters{
    
    if(radFilter){
        [_videoCamera removeTarget:radFilter];
        [radFilter removeTarget:imageView];
        radFilter=nil;
    }
    
    
    radFilter = [[GPUImageRadiationDetectionFilter alloc] init];
    [_videoCamera addTarget:radFilter];
    
    [radFilter addTarget:imageView];

    
}

- (void)configureUI {
    RACSignal *activeFormatSignal = [RACObserve(self, videoCamera.inputCamera.activeFormat) ignore:nil];
    
    [[activeFormatSignal ignore:nil] subscribeNext:^(id x) {
        [self setupFilters];
    }];
    
    RACSignal *isoSignal = RACObserve(self.videoCamera.inputCamera, ISO);
    
    RACSignal *focusModeSignal = [RACObserve(self.videoCamera.inputCamera, focusMode) map:^id(id value) {
        return @[@"Locked",@"Auto",@"Continuous",][[value integerValue]];
    }];
    
    RACSignal *smoothAutoFocusSignal = [[RACSignal combineLatest:@[
                                                                   RACObserve(self.videoCamera.inputCamera,smoothAutoFocusSupported),
                                                                   RACObserve(self.videoCamera.inputCamera,smoothAutoFocusEnabled)
                                                                   ]]
                                        map:^id(RACTuple *tuple) {
                                            
                                            BOOL smoothAutoFocusSupported = [tuple.first boolValue];
                                            BOOL smoothAutoFocusEnabled = [tuple.second boolValue];
                                            
                                            if (!smoothAutoFocusSupported) {
                                                return @"Not Supported";
                                            }
                                            
                                            return smoothAutoFocusEnabled?@"YES":@"NO";
                                            
                                        }];
    
    RACSignal *autoFocusRangeRestrictionSignal = [[RACSignal combineLatest:@[
                                                                             RACObserve(self.videoCamera.inputCamera,autoFocusRangeRestrictionSupported),
                                                                             RACObserve(self.videoCamera.inputCamera,autoFocusRangeRestriction)
                                                                             ]]map:^id(RACTuple *tuple) {
        
        BOOL autoFocusRangeRestrictionSupported = [tuple.first boolValue];
        
        if (!autoFocusRangeRestrictionSupported) {
            return @"Not Supported";
        }
        
        AVCaptureAutoFocusRangeRestriction autoFocusRangeRestriction = [tuple.second integerValue];
        return @[@"None",@"Near",@"Far"][autoFocusRangeRestriction];
        
    }];
    
    
    RACSignal *exposureSignal = [[RACSignal combineLatest:@[
                                                            RACObserve(self.videoCamera.inputCamera, exposureMode),
                                                            RACObserve(self.videoCamera.inputCamera, exposureDuration),
                                                            RACObserve(self.videoCamera.inputCamera, activeFormat.minExposureDuration),
                                                            RACObserve(self.videoCamera.inputCamera, activeFormat.maxExposureDuration),
                                                            RACObserve(self.videoCamera.inputCamera, exposureTargetBias)
                                                            
                                                            
                                                            
                                                            ]] map:^id(RACTuple *tuple) {
        
        NSString *mode = @[@"Locked",@"Auto",@"Continuous",@"Custom",][[tuple.first integerValue]];
        Float64 exposureDuration = CMTimeGetSeconds([tuple.second CMTimeValue])*1000;
        Float64 minExposureDuration = CMTimeGetSeconds([tuple.third CMTimeValue])*1000;
        Float64 maxExposureDuration = CMTimeGetSeconds([tuple.fourth CMTimeValue])*1000;
        float exposureTargetBias = [tuple.fifth floatValue];
        
        return [NSString stringWithFormat:@"Expose Mode: %@\nExposure Duration: %.2fms\nExposure Range: %.2fms-%.0fms\n\n", mode,exposureDuration,minExposureDuration,maxExposureDuration];
    }];
    
    
    RACSignal *WBModeSignal = [RACObserve(self.videoCamera.inputCamera, whiteBalanceMode) map:^id(id value) {
        return @[@"Locked",@"Auto",@"Continuous",@"Custom",][[value integerValue]];
    }];
    
    
    RACSignal *combineSignal = [RACSignal combineLatest:@[activeFormatSignal,
                                                          isoSignal,
                                                          focusModeSignal,
                                                          smoothAutoFocusSignal,
                                                          autoFocusRangeRestrictionSignal,
                                                          exposureSignal,
                                                          WBModeSignal,
                                                          RACObserve(self.videoCamera.inputCamera, videoZoomFactor),
                                                          RACObserve(self.videoCamera.inputCamera, lensPosition),
                                                          RACObserve(self.videoCamera.inputCamera, videoHDREnabled)
                                                          ]];
    
    RAC(statusLabel,text) = [combineSignal map:^id(RACTuple *tuple) {
        
        AVCaptureDeviceFormat *format = tuple.first;
        
        CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions(format.formatDescription);
        AVFrameRateRange *fpsRange = format.videoSupportedFrameRateRanges.firstObject;
        
        NSString *focusModeString = tuple.third;
        NSString *smoothAutoFocusString = tuple.fourth;
        NSString *autoFocusRangeRestrictionString = tuple.fifth;
        NSString *exposureString = [tuple objectAtIndex:5];
        NSString *whiteBalanceModeString = [tuple objectAtIndex:6];
        CGFloat videoZoomFactor = [[tuple objectAtIndex:7] floatValue];
        float lensPosition = [[tuple objectAtIndex:8] floatValue];
        BOOL videoHDREnabled = [[tuple objectAtIndex:9] boolValue];
        
        //WB Gains:-(max:-)\nWorld Device WB Gains:-
        
        return [NSString stringWithFormat:@"%@\nLens Aperture: %.2f\nDimensions: %dx%d\nFPS:%.0f-%.0f\nISO: %.0f\n\nFocus Mode: %@\nSmooth AF: %@\nAF Range Restriction: %@\n\n%@WB Mode:%@\n\n\nVideo Zoom Factor: %.0f\nLens Position: %.3f\n\nHDR: %@",
                self.videoCamera.inputCamera.localizedName,
                self.videoCamera.inputCamera.lensAperture,
                dimensions.width, dimensions.height,
                fpsRange.minFrameRate,fpsRange.maxFrameRate,
                self.videoCamera.inputCamera.ISO,
                focusModeString,
                smoothAutoFocusString,
                autoFocusRangeRestrictionString,
                exposureString,
                whiteBalanceModeString,
                videoZoomFactor,
                lensPosition,
                videoHDREnabled?@"Enabled":@"Disabled"
                ];
    }];
    
    
    [[resetBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [radFilter setNeedReset];
    }];
}

- (IBAction)toggleSettings:(id)sender {
    settingsContainerHeightConstraint.constant= settingsContainerHeightConstraint.constant==44?300:44;
    
    [UIView animateWithDuration:.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"settings"]) {
        UINavigationController *nc= (UINavigationController *) segue.destinationViewController;
        nc.view.backgroundColor=[UIColor clearColor];
        CaptureDeviceSettingsViewController *settingsController = (CaptureDeviceSettingsViewController *) nc.visibleViewController;
        
        RAC(settingsController, device) = RACObserve(self, videoCamera.inputCamera);
    }
}

@end
