//
//  GPUImageRadFilter.m
//  RadiationDemo
//
//  Created by Oleksandr Izvekov on 8/16/15.
//  Copyright (c) 2015 Oleksandr Izvekov. All rights reserved.
//

#import "GPUImageMaximumFilter_.h"

typedef struct {
    NSUInteger r, g, b, a;
} RGBAColor;



NSString *const kGPUImageRadFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;

 
 void main()
 {
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     gl_FragColor = textureColor;
 }
 );


@implementation GPUImageMaximumFilter_
{
    GLint texelWidthUniform, texelHeightUniform;
    
    NSUInteger numberOfStages;
    
    GLubyte *rawImagePixels;
    CGSize finalStageSize;
    
    GPUVector3* max;
    GPUVector3* max_1;
}



#pragma mark -
#pragma mark Initialization and teardown

- (id)init;
{
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageRadFragmentShaderString]))
    {
        return nil;
    }
    
    max = NULL;
    max_1 = NULL;
    
    texelWidthUniform = [filterProgram uniformIndex:@"texelWidth"];
    texelHeightUniform = [filterProgram uniformIndex:@"texelHeight"];
    finalStageSize = CGSizeMake(1.0, 1.0);
    
    __unsafe_unretained GPUImageMaximumFilter_ *weakSelf = self;
    [self setFrameProcessingCompletionBlock:^(GPUImageOutput *filter, CMTime frameTime) {
        [weakSelf extractMaximumsAtFrameTime:frameTime];
    }];
    
    
    return self;
}

#pragma mark -
#pragma mark Managing the display FBOs

- (void)renderToTextureWithVertices:(const GLfloat *)vertices textureCoordinates:(const GLfloat *)textureCoordinates;
{
    if (self.preventRendering)
    {
        [firstInputFramebuffer unlock];
        return;
    }
    
    outputFramebuffer = nil;
    [GPUImageContext setActiveShaderProgram:filterProgram];
    
    glVertexAttribPointer(filterPositionAttribute, 2, GL_FLOAT, 0, 0, vertices);
    glVertexAttribPointer(filterTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
    
    GLuint currentTexture = [firstInputFramebuffer texture];
    
    NSUInteger reduction = 1;
    
    CGSize currentStageSize = CGSizeMake(floor(inputTextureSize.width / reduction), floor(inputTextureSize.height /reduction));
    
    //    NSLog(@"currentStageSize: %@ -> %@", NSStringFromCGSize(inputTextureSize), NSStringFromCGSize(currentStageSize) );
    
    if ( (currentStageSize.height < 2.0) || (currentStageSize.width < 2.0) )
    {
        // A really small last stage seems to cause significant errors in the average, so I abort and leave the rest to the CPU at this point
        //                currentStageSize.height = 2.0; // TODO: Rotate the image to account for this case, which causes FBO construction to fail
    }
    
    [outputFramebuffer unlock];
    outputFramebuffer = [[GPUImageContext sharedFramebufferCache] fetchFramebufferForSize:currentStageSize textureOptions:self.outputTextureOptions onlyTexture:NO];
    [outputFramebuffer activateFramebuffer];
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, currentTexture);
    
    glUniform1i(filterInputTextureUniform, 2);
    
    glUniform1f(texelWidthUniform, 0.5 / currentStageSize.width);
    glUniform1f(texelHeightUniform, 0.5 / currentStageSize.height);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    currentTexture = [outputFramebuffer texture];
    finalStageSize = currentStageSize;
    [firstInputFramebuffer unlock];
}


- (void)extractMaximumsAtFrameTime:(CMTime)frameTime;
{
    runSynchronouslyOnVideoProcessingQueue(^{
        // we need a normal color texture for averaging the color values
        NSAssert(self.outputTextureOptions.internalFormat == GL_RGBA, @"The output texture internal format for this filter must be GL_RGBA.");
        NSAssert(self.outputTextureOptions.type == GL_UNSIGNED_BYTE, @"The type of the output texture of this filter must be GL_UNSIGNED_BYTE.");
        
        NSLog(@"%@", NSStringFromCGSize(finalStageSize));
        NSUInteger totalNumberOfPixels = round(finalStageSize.width * finalStageSize.height);
        
        if (rawImagePixels == NULL)
        {
            rawImagePixels = (GLubyte *)malloc(totalNumberOfPixels * 4);
        }
        
        [GPUImageContext useImageProcessingContext];
        [outputFramebuffer activateFramebuffer];
        glReadPixels(0, 0, (int)finalStageSize.width, (int)finalStageSize.height, GL_RGBA, GL_UNSIGNED_BYTE, rawImagePixels);
        
        
        if(max == NULL || max_1 == NULL){
            max = (GPUVector3 *)malloc(totalNumberOfPixels * sizeof(GPUVector3));
            max_1 = (GPUVector3 *)malloc(totalNumberOfPixels * sizeof(GPUVector3));
            
            NSUInteger byteIndex = 0;
            
            for (NSUInteger currentPixel = 0; currentPixel < totalNumberOfPixels; currentPixel++)
            {
                NSUInteger r = rawImagePixels[byteIndex++];
                NSUInteger g = rawImagePixels[byteIndex++];
                NSUInteger b = rawImagePixels[byteIndex++];
                NSUInteger a = rawImagePixels[byteIndex++];
                
                max[currentPixel] = (GPUVector3){.one=(CGFloat)r/255.0,.two=(CGFloat)g/255.0,.three=(CGFloat)b/255.0};
                max_1[currentPixel] = (GPUVector3){.one=0,.two=0,.three=0};
                
            }
        }else{
            
        }
        
    
        
        
        //        NSUInteger overSignalThreshold = 0;
        //        float maxSignal = 0;
        
        GLKVector4 max = GLKVector4Make(0, 0, 0, 0);
        GLKVector4 max_1 = GLKVector4Make(0, 0, 0, 0);
        
        NSUInteger byteIndex = 0;
        
        NSLog(@"totalNumberOfPixels: %lu",(unsigned long)totalNumberOfPixels);
        
        for (NSUInteger currentPixel = 0; currentPixel < totalNumberOfPixels; currentPixel++)
        {
            NSUInteger r = rawImagePixels[byteIndex++];
            NSUInteger g = rawImagePixels[byteIndex++];
            NSUInteger b = rawImagePixels[byteIndex++];
            NSUInteger a = rawImagePixels[byteIndex++];
            
            GLKVector4 value = GLKVector4Make(r,g,b,a);
            
            max_1 = GLKVector4Maximum(value, max_1);
            
            GLKVector4 nuMax = GLKVector4Maximum(max, max_1);
            GLKVector4 nuMin = GLKVector4Minimum(max, max_1);
            
            max = nuMax;
            max_1 = nuMin;
            
            
        }
        
//        NSLog(@"max: %@", NSStringFromGLKVector4(max));
//        NSLog(@"max_1: %@", NSStringFromGLKVector4(max_1));
        
        if (_colorMaximumProcessingFinishedBlock != NULL)
        {
            
            GPUVector4 maxV;
            maxV.one = (CGFloat)max.r/255.0;
            maxV.two = (CGFloat)max.g/255.0;
            maxV.three = (CGFloat)max.b/255.0;
            maxV.four = (CGFloat)max.a/255.0;
            
            GPUVector4 max_1V;
            max_1V.one = (CGFloat)max_1.r/255.0;
            max_1V.two = (CGFloat)max_1.g/255.0;
            max_1V.three = (CGFloat)max_1.b/255.0;
            max_1V.four = (CGFloat)max_1.a/255.0;
            
            _colorMaximumProcessingFinishedBlock(maxV, max_1V, frameTime);
        }
    });
}




@end
